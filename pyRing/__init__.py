# Get the version number from git tag
from pkg_resources import get_distribution, DistributionNotFound
try:
    __version__ = get_distribution(__name__).version
except(DistributionNotFound, AssertionError):
    # package is not installed
    __version__ = "dev"

