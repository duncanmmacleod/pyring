.. pyRing documentation master file, created by
   sphinx-quickstart on Fri Jun 25 19:38:09 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyRing's documentation!
-----------------------------------

.. image:: pyRing_docs_image.pdf
  :width: 650

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   install_and_run
   publications
   waveforms
   basic_usage
   contributing
   modules

.. include:: introduction.rst
.. include:: install_and_run.rst
.. include:: publications.rst
.. include:: waveforms.rst
.. include:: basic_usage.rst
.. include:: contributing.rst

Indices and tables
--------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
