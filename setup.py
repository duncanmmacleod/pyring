from os import path
import numpy
from setuptools import setup
from setuptools import Extension
import platform
WINDOWS = platform.system().lower() == "windows"

# check whether user has Cython
try:
    import Cython
    suff = '.pyx'
    have_cython = True
except ImportError:
    have_cython = False
    suff = '.c'

libraries = [] if WINDOWS else ["m"]

ext_modules=[
             Extension("pyRing.waveform",
                       sources=["pyRing/waveform"+suff],
                       libraries=libraries,
                       extra_compile_args=["-O3","-ffast-math"],
                       include_dirs=[numpy.get_include(),'pyRing']
                       ),
             Extension("pyRing.likelihood",
                       sources=["pyRing/likelihood"+suff],
                       libraries=libraries,
                       extra_compile_args=["-O3","-ffast-math"],
                       include_dirs=[numpy.get_include(),'pyRing']
                       ),
             Extension("pyRing.eob_utils",
                       sources=["pyRing/eob_utils"+suff],
                       libraries=libraries,
                       extra_compile_args=["-O3","-ffast-math"],
                       include_dirs=[numpy.get_include(),'pyRing']
                       )
             ]

if have_cython:
    # If we have cython, recompile the pyx files as required
    from Cython.Build import cythonize
    ext_modules = cythonize(ext_modules, language_level = "3")

# Get the long description from the relevant file
here = path.abspath(path.dirname(__file__))
with open(path.join(here, 'docs/introduction.rst'), encoding='utf-8') as f:
        long_description = f.read()

with open("requirements.txt") as requires_file:
    requirements = requires_file.read().split("\n")

setup(
      name             = "pyRing",
      description      = 'pyRing: Time-domain ringdown parameter estimation',
      long_description = long_description,
      author           = 'Gregorio Carullo, Walter Del Pozzo, Max Isi, Danny Laghi, John Veitch',
      author_email     = 'gregorio.carullo@ligo.org, walter.delpozzo@ligo.org, max.isi@ligo.org, danny.laghi@ligo.org, john.veitch@ligo.org',
      packages         = ['pyRing'],
      package_data     = {"pyRing":["*.c","*.pyx","*.pxd"]},
      ext_modules      = ext_modules,
      python_requires  = '>=3',
      include_dirs     = [numpy.get_include()],
      install_requires = requirements,
      setup_requires   = ['numpy','cython','setuptools_scm'],
      entry_points     = {'console_scripts': ['pyRing=pyRing.pyRing:main']}
)
